**********
Components
**********

Tables
======

OLAT provides the ``AbstractFlexiTableRenderer`` for presenting tabular information.
It comes with two implementations: ``FlexiTableClassicRenderer`` and
``FlexiTableCustomRenderer``. While the ``Classic`` renderer produces a HTML
``table`` the ``Custom`` renderer generates a ``div`` structure with is
recognized as a *table* by the underlying bootstrap frontend framework.


You need several other components to initialize a table renderer:

.. code-block:: java

   FlexiTableColumnModelImpl ftcmi = new FlexiTableColumnModelImpl();
   CoursesTableDataModel tableModel = new CoursesTableDataModel(ftcmi);
   List<CourseStatEntry> rows = new ArrayList<>();
   rows.add(new CourseStatEntry());

   tableModel.setObjects(rows);
   FlexiTableElementImpl fte = new FlexiTableElementImpl(wControl, "foobar", translator, tableModel);
   FlexiTableComponent source = new FlexiTableComponent(fte);

   FlexiTableClassicRenderer ftcr = new FlexiTableClassicRenderer();
   Renderer renderer = Renderer.getInstance(panel, translator, ubu, renderResult, gsettings, csrfToken);
   ftcr.render(renderer, sb, source, ubu, translator, renderResult, null);


#. The ``Panel (panel)`` is the collector for the rendered components.
#. The ``Translator (translator)`` is the localization component for labels.
#. The ``ÙRLBuilder (ubu)`` is responsible for putting together OLAT internal links.
#. The ``RenderResult (renderResult)`` keeps track of rendering layers and reports rendering errors.
#. The ``GlobalSettings (gsettings)`` manages global flags for handling AJAX and other elements.
#. The ``CSRF Token (csrfToken)`` is a string to prevent injecting a non authorised component.
#. The ``StringOutput (sb)`` contains the generated HTML.
#. The ``FlexiTableComponent (source)`` connects the table data with the renderer. It is responsible
   for choosing the type *classic* or *custom*.
#. The ``FlexiTableColumnModelImpl (ftcmi)`` does not add much spice to the sauce. It is mainly a link
   between the table data model and its column representation.
#. An interesting part is the table data model:

   .. code-block:: java

      public class CoursesTableDataModel extends DefaultFlexiTableDataModel<CourseStatEntry>
   	  implements SortableFlexiTableDataModel<CourseStatEntry> {

        @Override
        public void setObjects(List<U> objects) {
          this.rows = new ArrayList<>(objects);
        }
      }

   It handles the actual tabular data in the ``rows`` attribute.
#. Another really important part plays the ``FlexiTableElementImpl``. It is responsible for 
   the sorting, batching, breadcrumbs and other aspects of modifying the table. It also
   manages the connection to the parent form and the window control.


#. For the ``Custom`` renderer you need an additional row renderer:

   .. code-block:: java
   
      String velocity_root = Util.getPackageVelocityRoot(AssessmentModeOverviewListController.class);
      VelocityContainer row = new VelocityContainer(
        "unittest", "vc_assessment_mode_overview_row",
        velocity_root + "/assessment_mode_overview_row.html", translator, controller);

      FlexiTableElementImpl fte = new FlexiTableElementImpl(wControl, "foobar", translator, tableModel);
      fte.setRowRenderer(row, null);



