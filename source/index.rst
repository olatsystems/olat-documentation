.. OpenOlat Development Documentation documentation master file, created by
   sphinx-quickstart on Sat Apr 25 07:06:05 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OLAT development documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Configuration.rst
   Testing.rst
   Acceptance_Testing.rst
   Architecture.rst
   Components.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
