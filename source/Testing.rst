*******
Testing
*******

Prerequisites
=============

You need a PostgreSQL database running. You can do this either with a local database or with docker.

.. code-block:: bash

   docker run -e POSTGRES_PASSWORD=postgres -p 5432:5432 postgres:11

Using an IDE
============

These examples are based on Intellij IDE but Eclipse should also work.

Before running single test in your IDE you need to make sure the test database is created
and provided with the necessary values. You can do this with the following maven command:


.. code-block:: bash

   mvn -Ptomcat -Dwith-postgresql pre-integration-test

.. note:: To have a new test recogized by the test runner you have to add it to the
  *src/test/java/org/olat/test/AllTestsJunit4.java* file.


Writing Tests
=============

OLAT depends on JUnit4 for writing and runnint tests. To write functional tests that
depend on a full OLAT you may extend from ``OlatTestCase``:

.. code-block:: java

   public class FlexiTableClassicRendererTest extends OlatTestCase {

   @Before
   public void setup() {
      // ...
   }

   @Test
   public void test_helloworld() {
     assertThat("hello world").contains("hello");
   }


Most of the internal API of OLAT methods depend on a translator. Usally this information
is taken from the request. To have a minimal translator in tests, create one with:

.. code-block:: java

   Translator translator = new KeyTranslator(Locale.ENGLISH);



