*************
Configuration
*************

Requirements
============

* Java JDK 8 (preferably AdoptOpenJDK)
* ImageMagick (https://imagemagick.org/index.php)
* Database (preferably PostgreSQL, MariaDB/MySQL are also possible)

Optional

* IDE (IntelliJ or Eclipse)
* Docker

Install
=======

An installation tutorial is here: https://www.openolat.com/fileadmin/adminwiki/QW5vdGhlcl9JbnN0YWxsYXRpb25fR3VpZGVfUG9zdGdyZVNRTA==.html

An important artifact for configuring OLAT is `olat.local.properties`. It is
used to define db access, some application level module configuration like ldap, shibboleth authentication, etc.

It needs to be placed in the class path to start the server.

.. note:: For testing there are some `olat.local.propertis` prepared by OpenOlat. They can be found in src/test/profile/*


You can reuse one of the testing profiles to run OLAT in your IntelliJ 

.. code-block:: bash

  cp src/test/profile/postgresql/olat.local.properties target/classes

Maven Properties
================

TBD

OLAT Properties
===============

TBD


Troubleshooting
===============

Here are some tipps and recipies if you encounter issues when runing OLAT.

Incremental compile
-------------------

If you see ``[INFO] Changes detected - recompiling the module!`` messages on maven compile you can 

.. code-block:: xml

  <plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-compiler-plugin</artifactId>
    ...
    <configuration>
    ...
      <useIncrementalCompilation>false</useIncrementalCompilation>
    </configuration>
  </plugin>


Tomcat cache
------------

If you see ``19-May-2020 10:17:40.934 WARNUNG [RMI TCP Connection(4)-127.0.0.1] org.apache.catalina.webresources.Cache.getResource Unable to add the resource at [/WEB-INF/classes/org/olat/core/gui/control/generic/wizard/_i18n/LocalStrings_fr.properties] to the cache for web application [/openolat_lms_war_exploded] because there was insufficient free space available after evicting expired cache entries - consider increasing the maximum size of the cache`` flooding your tomcat catalina.log at startup increase the cache of tomcat.

In your ``$CATALINA_BASE/conf/context.xml`` add block below before ``</Context>``

.. code-block:: xml

  <Resources cachingAllowed="true" cacheMaxSize="100000" />


See this `Stackoverflow entry <https://stackoverflow.com/questions/26893297/tomcat-8-throwing-org-apache-catalina-webresources-cache-getresource-unable-to/37986333>`_ for more information.
